﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Newtonsoft.Json;
using WebApplication2.Models;
using WebApplication2.Services;

namespace WebApplication2.Controllers
{
    public class HomeController : Controller
    {
        private IWebShopService service;
        private IAccountServices acservice;
        public ISession session;

        public HomeController(IWebShopService Service, IAccountServices Acservice, IHttpContextAccessor accessor)
        {
            service = Service;
            acservice = Acservice;
            session = accessor.HttpContext.Session;
        }

        public override void OnActionExecuted(ActionExecutedContext context)
        {
            base.OnActionExecuted(context);
        }

        public IActionResult Index()
        {
            return View("Index");
        }

        public IActionResult Categories()
        {
            ViewBag.Categories = service.Categories().ToList();
            ViewBag.Randoms = service.GetRandomProductsForCategories().ToList();

            return View("Categories");
        }

        public IActionResult Products(string sortOrder,string currentFilter,
            string searchString, int? pageNumber, int catId)
        {
            ViewData["CurrentSort"] = sortOrder;
            ViewData["NetPriceSortParm"] = String.IsNullOrEmpty(sortOrder) ? "netprice_desc" : "";
            ViewData["ProducerSortParm"] = sortOrder == "producer_asc" ? "producer_desc" : "producer_asc";

            if (searchString != null)
            {
                pageNumber = 1;
            }
            else
            {
                searchString = currentFilter;
            }
            ViewData["CurrentFilter"] = searchString;

            var products = from p in service.GetProductsByCategory(catId) select p;

            if (!String.IsNullOrEmpty(searchString))
            {
                products = products.Where(p => p.Name.Contains(searchString));
            }

            switch (sortOrder)
            {
                case "producer_asc":
                    products = products.OrderBy(p => p.Producer);
                    break;
                case "producer_desc":
                    products = products.OrderByDescending(p => p.Producer);
                    break;
                case "netprice_desc":
                    products = products.OrderByDescending(p => p.NetPrice);
                    break;
                default:
                    products = products.OrderBy(p => p.NetPrice);
                    break;
            }

            int pageSize = 10;
            ViewBag.Id = catId;
            return View(PaginatedList<Product>.Create(products.AsQueryable(), pageNumber ?? 1, pageSize));
        }

        public IActionResult Details(int prodId, int categoryID)
        {
            List<Product> cart;
            if (session.GetString("cart") == null)
            {
                cart = new List<Product>();
            }
            else
            {
                cart = JsonConvert.DeserializeObject<List<Product>>(session.GetString("cart"));
            }

            ViewBag.CountInCart = cart.Where(p => p.Id == prodId).Count();
            ViewBag.Product = service.Products().Where(p => p.Id == prodId).FirstOrDefault();
            
            if(service.Products().Where(p=> p.Id == prodId).FirstOrDefault().StockNumber - ViewBag.CountInCart <= 0)
            {
                ViewBag.ShowButton = false;
            }
            else
            {
                ViewBag.ShowButton = true;
            }

            if(service.Products().Where(p => p.Id == prodId).FirstOrDefault().isAvailable == true)
            {
                ViewBag.Available = true;
            }
            else if(service.Products().Where(p => p.Id == prodId).FirstOrDefault().isAvailable == false)
            {
                ViewBag.Available = false;
            }

            return View("Details");
        }

        public IActionResult CartPage()
        {
            if (session.GetString("cart") == null)
            {
                ViewBag.Cart = new List<Product>().ToList();
            }
            else
            {
                ViewBag.Cart = JsonConvert.DeserializeObject<List<Product>>(session.GetString("cart")).ToList();
            }
            if (session.GetInt32("total") == null)
            {
                ViewBag.Total = 0;
            }
            else
            {
                ViewBag.Total = (int)session.GetInt32("total");
            }
            return View("CartPage");
        }

        public IActionResult AddToCart(int? prodId, int? catId)
        {
            if (prodId == null || catId == null)
            {
                return NotFound();
            }
            List<Product> cart;
            if(session.GetString("cart") == null)
            {
                cart = new List<Product>();
            }
            else
            {
                cart = JsonConvert.DeserializeObject<List<Product>>(session.GetString("cart"));
            }

            Product prod = service.GetProductsByCategory((int)catId).Where(p => p.Id == (int)prodId).FirstOrDefault();
            cart.Add(prod);
            int total;
            if (session.GetInt32("total") == null)
            {
                total = prod.NetPrice;
            }
            else
            {
                total = (int)session.GetInt32("total");
                total += prod.NetPrice;
            }

            session.SetInt32("total", total);
            session.SetString("cart", JsonConvert.SerializeObject(cart, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    }));
            
            ViewBag.Product = prod;
            ViewBag.CountInCart = cart.Where(p => p.Id == prodId).Count();
            ViewBag.Cart = cart;
            if (service.Products().Where(p => p.Id == prodId).FirstOrDefault().StockNumber - ViewBag.CountInCart <= 0)
            {
                ViewBag.ShowButton = false;
            }
            else
            {
                ViewBag.ShowButton = true;
            }

            if (service.Products().Where(p => p.Id == prodId).FirstOrDefault().isAvailable == true)
            {
                ViewBag.Available = true;
            }
            else if (service.Products().Where(p => p.Id == prodId).FirstOrDefault().isAvailable == false)
            {
                ViewBag.Available = false;
            }

            return View("Details", cart);
        }

        public IActionResult RemoveFromCart(int? prodId)
        {
            if (prodId == null)
            {
                return NotFound();
            }

            List<Product> cart = JsonConvert.DeserializeObject<List<Product>>(session.GetString("cart"));
            cart.Remove(cart.FirstOrDefault(p => p.Id == (int)prodId));
            session.SetString("cart", JsonConvert.SerializeObject(cart, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    }));
            int total = (int)session.GetInt32("total") - service.Products().Where(p=> p.Id == (int)prodId).FirstOrDefault().NetPrice;
            session.SetInt32("total", total);
            ViewBag.Total = total;
            ViewBag.Cart = JsonConvert.DeserializeObject<List<Product>>(session.GetString("cart")).ToList();
            return View("CartPage");
        }

        public IActionResult RemoveAllProdFromCart()
        {
            List<Product> cart = JsonConvert.DeserializeObject<List<Product>>(session.GetString("cart"));
            cart.RemoveAll(cart => cart.Id != -1);
            session.SetString("cart", JsonConvert.SerializeObject(cart, Formatting.Indented,
                    new JsonSerializerSettings
                    {
                        PreserveReferencesHandling = PreserveReferencesHandling.Objects
                    }));
            session.SetInt32("total", 0);
            ViewBag.Total = 0;
            ViewBag.Cart = JsonConvert.DeserializeObject<List<Product>>(session.GetString("cart")).ToList();
            return View("CartPage");

        }

        [HttpGet]
        public IActionResult FormPage(int? total)
        {
            if(total == null)
            {
                return NotFound();
            }
            OrderViewModel order = new OrderViewModel();
            order.Total = (int)total;

            return View("FormPage", order);
        }

        [HttpPost]
        public IActionResult FormPage(OrderViewModel order)
        {
            if (!ModelState.IsValid)
                return View("FormPage", order);

            if (session.GetString("cart") == null) //at pagereload the cart is empty
            {
                return View("Index");
            }

            if (!service.SaveOrder(order, JsonConvert.DeserializeObject<List<Product>>(session.GetString("cart")).ToList()))
            {
                ModelState.AddModelError("", "The saving of the order failed, please try again");
                return View("Result", order);
            }

            order.Total = (int)session.GetInt32("total");

            ViewBag.Message = "The order has been successfully saved";

            session.Clear();
            return View("Result", order);
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
