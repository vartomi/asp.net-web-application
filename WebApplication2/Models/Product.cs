using System.Collections.Generic;

namespace WebApplication2.Models
{
    public class Product
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Producer { get; set; }
        public int ModelNumber { get; set; }
        public string Description { get; set; }
        public int NetPrice { get; set; }
        public int StockNumber { get; set; }
        public bool isAvailable { get; set; }

        public int CategoryId { get; set; }
        public virtual Category Category { get; set; }

        public ICollection<ProductOrder> ProductOrders { get; set; }
    }
}