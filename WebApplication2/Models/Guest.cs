﻿namespace WebApplication2.Models
{
    public class Guest
    {
        public int ID { get; set; }
        public string UserName { get; set; }
        public string UserPassword { get; set; }

        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string EmailAddress { get; set; }
    }
}
