﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using WebApplication2.Models;

namespace WebApplication2.Services
{
    public class WebShopService : IWebShopService
    {
        private WebShopContext context;
        private IAccountServices accountService;

        public WebShopService(WebShopContext con, IAccountServices acs)
        {
            context = con;
            accountService = acs;
        }

        public IEnumerable<Category> Categories()
        {
            return context.Categories.ToList();
        }

        public IEnumerable<Product> Products()
        {
            return context.Products.
                Include(p=> p.Category).ToList();
        }

        public IEnumerable<Product> GetProductsByCategory(int catId)
        {
            return context.Products
                .Include(p => p.Category)
                .Where(p => p.CategoryId == catId).ToList();
        }

        public IEnumerable<Product> GetRandomProductsForCategories()
        {
            List<Product> products = Products().OrderBy(a => Guid.NewGuid()).ToList();
            List<Category> cats = Categories().ToList();
            List<Product> res = new List<Product>();
            foreach(Category cat in cats)
            {
                res.Add(products.Where(p => p.Category == cat).DefaultIfEmpty(null).First());
            }

            if (res.Count != cats.Count()) throw new Exception();

            return res;
        }

        public Boolean SaveOrder(OrderViewModel order, List<Product> cart)
        {
            if (!Validator.TryValidateObject(order, new ValidationContext(order, null, null), null))
                return false;

            Guest guest = new Guest
            {
                Name = order.Name,
                Address = order.Address,
                PhoneNumber = order.PhoneNumber,
                EmailAddress = order.EmailAddress
            };

            context.Guests.Add(guest);

            try
            {
                context.SaveChanges();
            }
            catch (Exception)
            {
                return false;
            }

            Order o = new Order
            {
                guestId = guest.ID,
                Name = order.Name,
                Address = order.Address,
                PhoneNumber = order.PhoneNumber,
                Email = order.EmailAddress,
                isSaved = true
            };
            
            List<ProductOrder> prodOrders = new List<ProductOrder>();
            int total = 0;
            foreach (Product prod in cart)
            {
                total += prod.NetPrice;
                ProductOrder po = new ProductOrder { OrderId = o.Id, ProductId = prod.Id, ProductCount = 1};

                if (prodOrders.Any(po => po.ProductId == prod.Id))
                {
                    prodOrders.Where(i => i.ProductId == prod.Id).FirstOrDefault().ProductCount++;
                }
                else
                {
                    prodOrders.Add(po);
                }
            }

            o.Total = total;
            o.ProductOrders = prodOrders;

            context.Orders.Add(o);

            try
            {
                context.SaveChanges();
                RemoveProductsFromDb(o.ProductOrders);
            }
            catch (Exception)
            {
                return false;
            }

            return true;
        }

        private void RemoveProductsFromDb(ICollection<ProductOrder> prodOrders)
        {
            foreach (ProductOrder po in prodOrders)
            {
                context.Products.Where(p => p.Id == po.ProductId).FirstOrDefault().StockNumber -= po.ProductCount; 
            }

            try
            {
                context.SaveChanges();
            }
            catch (Exception)
            {
                throw new Exception();
            }
        }

    }
}
