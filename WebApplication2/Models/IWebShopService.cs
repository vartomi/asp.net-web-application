﻿using System.Collections.Generic;
using WebApplication2.Models;

namespace WebApplication2.Services
{
    public interface IWebShopService
    {
        IEnumerable<Category> Categories();
        IEnumerable<Product> Products();
        IEnumerable<Product> GetProductsByCategory(int catId);
        IEnumerable<Product> GetRandomProductsForCategories();
        bool SaveOrder(OrderViewModel order, List<Product> cart);
    }
}
