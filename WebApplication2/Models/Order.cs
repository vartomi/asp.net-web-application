﻿using System.Collections.Generic;

namespace WebApplication2.Models
{
    public class Order
    {
        public int Id { get; set; }
        public int guestId { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }
        public string Email { get; set; }
        public bool isCompleted { get; set; }
        public bool isSaved { get; set; }
        public int Total { get; set; }

        public ICollection<ProductOrder> ProductOrders { get; set; }
    }
}
