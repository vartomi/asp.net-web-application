﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;

namespace WebApplication2.Models
{
    public static class DbInitilalizer
    {
        public static void Initialize(IServiceProvider serviceProvider, string imageDirectory)
        {
            WebShopContext context = serviceProvider.GetRequiredService<WebShopContext>();

            // Create database if not exists based on current code-first model.
            context.Database.EnsureCreated();

            // Create / update database based on migration classes.
            // context.Database.Migrate();
            // Use only one of the above!

            if (context.Categories.Any())
            {
                return;
            }

            IList<Category> defaultCategories = new List<Category>();
            defaultCategories.Add(
                new Category
                {
                    Name = "Consumer electronics",
                    Products = new List<Product>()
                    {
                        new Product()
                        {
                            Producer = "Microsoft",
                            Name = "XBOX ONE",
                            ModelNumber = 1001,
                            Description = "XBOX Video Game Console",
                            NetPrice = 80000,
                            StockNumber = 10,
                            isAvailable = false
                        },
                        new Product()
                        {
                            Producer = "Sony",
                            Name = "PS4 500GB",
                            ModelNumber = 1002,
                            Description = "PS4 Video Game Console",
                            NetPrice = 70000,
                            StockNumber = 5,
                            isAvailable = false
                        },
                        new Product()
                        {
                            Producer = "Nintendo",
                            Name = "Nintendo Wii",
                            ModelNumber = 1003,
                            Description = "Wii Video Game Console",
                            NetPrice = 60000,
                            StockNumber = 8,
                            isAvailable = false
                        }
                    }
                });

            defaultCategories.Add(
                new Category
                {
                    Name = "IT",
                    Products = new List<Product>()
                    {
                        new Product()
                        {
                            Producer = "Lenovo",
                            Name = "Lenovo ThinkPad E540",
                            ModelNumber = 2001,
                            Description = "Lenovo ThinkPad E540 notebook",
                            NetPrice = 160000,
                            StockNumber = 2,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "Asus",
                            Name = "Asus ZenBook UX331FN",
                            ModelNumber = 2002,
                            Description = "Asus ZenBook UX331FN laptop",
                            NetPrice = 250000,
                            StockNumber = 7,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "HP",
                            Name = "HP 255",
                            ModelNumber = 2003,
                            Description = "HP 255 laptop",
                            NetPrice = 170000,
                            StockNumber = 7,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "Apple",
                            Name = "Apple MacBook Air 13",
                            ModelNumber = 2004,
                            Description = "Apple MacBook Air 13 laptop",
                            NetPrice = 280000,
                            StockNumber = 4,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "Apple",
                            Name = "Apple MacBook 16",
                            ModelNumber = 2005,
                            Description = "Apple MacBook 16\" laptop",
                            NetPrice = 1100000,
                            StockNumber = 15,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "Dell",
                            Name = "Dell Inspirion 5593",
                            ModelNumber = 2006,
                            Description = "Dell Inspirion 5593 laptop",
                            NetPrice = 210000,
                            StockNumber = 7,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "Lenovo",
                            Name = "Lenovo Yoga 11e",
                            ModelNumber = 2007,
                            Description = "Lenovo Yoga 11e laptop",
                            NetPrice = 80000,
                            StockNumber = 10000,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "Lenovo",
                            Name = "Lenovo ThinkPad x220",
                            ModelNumber = 2008,
                            Description = "Lenovo ThinkPad X220 laptop",
                            NetPrice = 60000,
                            StockNumber = 15,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "Dell",
                            Name = "Dell XPS 13",
                            ModelNumber = 2009,
                            Description = "Dell XPS 13 laptop",
                            NetPrice = 250000,
                            StockNumber = 2,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "ACER",
                            Name = "ACER ChromeBook 15",
                            ModelNumber = 2010,
                            Description = "ACER ChromeBook 15 laptop",
                            NetPrice = 220000,
                            StockNumber = 10,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "Huaweii",
                            Name = "Huaweii MateBook P",
                            ModelNumber = 2011,
                            Description = "Huaweii MateBook P laptop",
                            NetPrice = 260000,
                            StockNumber = 1,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "Xiaomi",
                            Name = "Xiaomi RedmiBook",
                            ModelNumber = 2012,
                            Description = "Xiaomi RedmiBook laptop",
                            NetPrice = 310000,
                            StockNumber = 6,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "ASUS",
                            Name = "ASUS VivoBook S13",
                            ModelNumber = 2013,
                            Description = "ASUS VivoBook S13 laptop",
                            NetPrice = 151000,
                            StockNumber = 5,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "HP",
                            Name = "HP ELITEBOOK",
                            ModelNumber = 2014,
                            Description = "HP ELITEBOOK laptop",
                            NetPrice = 90000,
                            StockNumber = 4,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "Lenovo",
                            Name = "Lenovo IdeaPad",
                            ModelNumber = 2015,
                            Description = "Lenovo IdeaPad D330",
                            NetPrice = 125000,
                            StockNumber = 10,
                            isAvailable = true
                        }
                    }
                });

            defaultCategories.Add(
                new Category
                {
                    Name = "Kitchen machines",
                    Products = new List<Product>()
                    {
                        new Product()
                        {
                            Producer = "Bosch",
                            Name = "Bosch KVG36LS",
                            ModelNumber = 3001,
                            Description = "Bosch KVG36LS refrigerator",
                            NetPrice = 130000,
                            StockNumber = 5,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "Bosch",
                            Name = "Bosch MUM48Ri",
                            ModelNumber = 3002,
                            Description = "Bosch MUM48Ri blender",
                            NetPrice = 37000,
                            StockNumber = 3,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "Bosch",
                            Name = "Bosch MUM582",
                            ModelNumber = 3003,
                            Description = "Bosch MUM582 blender",
                            NetPrice = 106000,
                            StockNumber = 4,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "Electorlux",
                            Name = "Electorlux EKK5135",
                            ModelNumber = 3004,
                            Description = "Electorlux EKK5135 combined stove",
                            NetPrice = 88000,
                            StockNumber = 8,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "Electorlux",
                            Name = "Electorlux EKC529",
                            ModelNumber = 3005,
                            Description = "Electorlux EKC529 electric stove",
                            NetPrice = 107000,
                            StockNumber = 13,
                            isAvailable = true
                        },
                        new Product()
                        {
                            Producer = "Electorlux",
                            Name = "Electorlux EKC513",
                            ModelNumber = 3006,
                            Description = "Electorlux EKC513 stove",
                            NetPrice = 89000,
                            StockNumber = 4,
                            isAvailable = true
                        }
                    }
                });

            Category cat = new Category
            {
                Name = "Many products",
                Products = new List<Product>()
            };

            for (int i = 0; i < 50; i++)
            {
                Product p = new Product
                {
                    Producer = "Producer",
                    Name = "Product " + i.ToString(),
                    ModelNumber = 4001 + i,
                    Description = "This product was created to test pagination",
                    NetPrice = 10000 - i,
                    StockNumber = 4,
                    isAvailable = true
                };
                cat.Products.Add(p);
            }

            defaultCategories.Add(cat);

            Category emptyCat = new Category
            {
                Name = "Empty category",
                Products = new List<Product>()
            };
            defaultCategories.Add(emptyCat);

            foreach (var categ in defaultCategories)
            {
                context.Categories.Add(categ);
            }

            context.SaveChanges();
        }
    }
}
