﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication2.Models
{
    public class OrderViewModel
    {
        [Required(ErrorMessage = "The name cannot be blank")]
        [StringLength(60, ErrorMessage = "Max 60 characters")]
        public string Name { get; set; }
        
        [Required(ErrorMessage = "The address cannot be blank")]
        public string Address { get; set; }
        
        [Required(ErrorMessage = "The telephone number cannot be blank")]
        [Phone(ErrorMessage = "Format is not valid")]
        [DataType(DataType.PhoneNumber)]
        public string PhoneNumber { get; set; }

        [Required(ErrorMessage = "The email address cannot be blank")]
        [EmailAddress(ErrorMessage ="Format is not valid")]
        [DataType(DataType.EmailAddress)]
        public string EmailAddress { get; set; }

        [DataType(DataType.Currency)]
        public int Total { get; set; }
    }
}
