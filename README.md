# Overview
The project is an ASP.NET web application created for my university studies. The application is a simplified version of a webshop selling electrical devices. The application allows the users to make orders for the items that are available in the store. This is located in the [WebApplication2](/WebApplication2/) folder.

# How to download the application
1. step: Navigate into the folder created for the gitlab repository. 
2. step: Clone the gitlab repository:

```shell
git clone https://gitlab.com/vartomi/asp.net-web-application.git
```

# How to run the debug version of the application
Running the application in this version requires more storage space (especially if Microsoft Visual Studio is not yet downloaded) but allows complete control over the application source code. If developing and running the application is needed, I would recommend to use the application this way.  

1. step: After cloning the repository, navigate into the cloned folder.
2. step: Execute the [WebApplication2.sln](/WebApplication2.sln) solution file. This will open a Microsoft Visual Studio instance.
3. step: Finally, start the application by pressing the <em>IIS Express</em> button with the green triangle.

# How to run the release version of the application
Running the application in this version requires some runtime dependencies to be downloaded but takes less storage space and runs faster than the debug version of the application. If only running the application is needed, I would recommend to use the application this way.

1. step: After cloning the repository, navigate into the cloned folder.
2. step: Next, navigate into the [Release/](/Release/) folder and extract [netcoreapp3.1.zip](/Release/netcoreapp3.1.zip) zip file.
3. step: After this open up a windows command prompt in the extracted folder and run the WebApplication2.exe file from the command prompt. After a few seconds the application is ready to be used. It is important to run the WebApplication2.exe file from the command prompt since it gives the links to download the required runtime dependencies.
4. step: Finally, open a browser and type in the route to the web application: http://localhost:5000.

# Description and functionalities of the application
At the <em>index page</em>, a welcome sign can be seen. On the top part of the application, the menu with 3 menu items is present. The menu can be reached from all pages of the application. The 3 menu items are <em>Home</em>, <em>Categories</em> and <em>Cart</em>. The list of the pages and their description is the following:
1. step: The <em>Home</em> menu item redirects the users to the <em>index page</em>.
2. step: By clicking on the <em>Categories</em> menu item, the users get redirected to the <em>categories page</em>, where the categories and one randomly selected product from each category is listed.
3. step: Selecting one category, the users get to the <em>products page</em> of the category. A maximum of 20 products per page are listed. The next or previous products can be listed by **paging**. The products can be **sorted** in both ways by clicking on the column header of the product producer or the product price columns.
4. step: To purchase a product, the users have to click on the product’s details in the <em>More details</em> column which leads them to the <em>product page</em>. Here all the information is listed about the product including its stock. If a product is active and is not out of stock, the users can buy it by **adding the product into their cart**. Otherwise, the users will see the following sentence displayed: "The product is currently not available (inactive)".
5. step: The content of the users carts can be observed by clicking on the <em>Cart</em> menu item which redirects them to the <em>cart page</em>. Multiple products can be added in different amounts into their carts. Here the users can **delete the products** from their carts one by one or by deleting the whole content of the cart.
6. step: When clicking on the <em>Confirm order</em> button, the users get redirected to the <em>form page</em>. Here the users have to fill in **their valid personal details**. After this to confirm the order, the users have to click on the <em>Confirm</em> button. 
7. step: If the confirmation of the order has been successful, the application saves the order to the database and informs the users about it.
